﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WorkBuggyBaseStation.SensorRepresentation
{
    /// <summary>
    /// Interaction logic for URSensorRepresentation.xaml
    /// </summary>
    public partial class URSensorRepresentation : UserControl
    {
        public URSensorRepresentation()
        {
            InitializeComponent();
        }



        public float MaxValue
        {
            get { return (float)GetValue(MaxValueProperty); }
            set { SetValue(MaxValueProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MaxValue.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MaxValueProperty =
            DependencyProperty.Register("MaxValue", typeof(float), typeof(URSensorRepresentation), new PropertyMetadata(100.0f, currMinMaxChanged));

        private static void currMinMaxChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var obj = d as URSensorRepresentation;
            if (obj == null)
                return;

            if (obj.MinValue >= obj.MaxValue)
                throw new ArgumentException($"Min value is greater or equal to max value {obj.MinValue} >= {obj.MaxValue}");

            obj.SensorRepresentation.Length = 100 - obj.Percentage(obj.CurrentValue);
            obj.SensorRepresentation.CurveRadius = ((100 - obj.Percentage(obj.CurrentValue)) / 100) * obj.BackingCone.CurveRadius;
            obj.SensorRepresentation.ConeFill = obj.GetSensorRepFillColour();
        }

        private Brush GetSensorRepFillColour()
        {
            var val = Percentage(CurrentValue);
            if (val >= 75)
            {
                return Brushes.Red;
            }
            else if (val >= 50)
            {
                return Brushes.Orange;
            }
            else if (val >= 25)
            {
                return Brushes.Yellow;
            }
            else
            {
                return Brushes.Green;
            }
        }



        public float SensorAngle
        {
            get { return (float)GetValue(SensorAngleProperty); }
            set { SetValue(SensorAngleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SensorAngle.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SensorAngleProperty =
            DependencyProperty.Register("SensorAngle", typeof(float), typeof(URSensorRepresentation), new PropertyMetadata(0.0f, sensorAngleChanged));

        private static void sensorAngleChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var obj = d as URSensorRepresentation;
            if (obj == null)
                return;

            obj.SensorContainer.RenderTransform = new RotateTransform(obj.SensorAngle, 50, 100);
        }

        public float CurrentValue
        {
            get { return (float)GetValue(CurrentValueProperty); }
            set { SetValue(CurrentValueProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CurrentValue.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CurrentValueProperty =
            DependencyProperty.Register("CurrentValue", typeof(float), typeof(URSensorRepresentation), new PropertyMetadata(0.0f, currMinMaxChanged));



        public float MinValue
        {
            get { return (float)GetValue(MinValueProperty); }
            set { SetValue(MinValueProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MinValue.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MinValueProperty =
            DependencyProperty.Register("MinValue", typeof(float), typeof(URSensorRepresentation), new PropertyMetadata(0.0f, currMinMaxChanged));
        
        private float Percentage(float val)
        {
            return (val / (MaxValue - MinValue)) * 100.0f;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            var obj = this;
            obj.SensorRepresentation.Length = 100 - obj.Percentage(obj.CurrentValue);
            obj.SensorRepresentation.CurveRadius = ((100 - obj.Percentage(obj.CurrentValue)) / 100) * obj.BackingCone.CurveRadius;
            obj.SensorRepresentation.ConeFill = obj.GetSensorRepFillColour();
        }
    }
}

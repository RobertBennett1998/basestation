﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WorkBuggyBaseStation.SensorRepresentation
{
    /// <summary>
    /// Interaction logic for URSensorRepresentation.xaml
    /// </summary>
    public partial class IRSensorRepresentation : UserControl
    {
        public IRSensorRepresentation()
        {
            InitializeComponent();
        }



        public float MaxValue
        {
            get { return (float)GetValue(MaxValueProperty); }
            set { SetValue(MaxValueProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MaxValue.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MaxValueProperty =
            DependencyProperty.Register("MaxValue", typeof(float), typeof(IRSensorRepresentation), new PropertyMetadata(100.0f, currMinMaxChanged));

        private static void currMinMaxChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var obj = d as IRSensorRepresentation;
            if (obj == null)
                return;

            if (obj.CurrentValue > obj.MaxValue)
                obj.CurrentValue = obj.MaxValue;

            if (obj.CurrentValue < obj.MinValue)
                obj.CurrentValue = obj.MinValue;

            if (obj.MinValue >= obj.MaxValue)
                throw new ArgumentException($"Min value is greater or equal to max value {obj.MinValue} >= {obj.MaxValue}");

            var perc = obj.Percentage((obj.CurrentValue));
            obj.SensorRect.Height = perc;
            obj.SensorRect.Fill = obj.GetSensorRepFillColour();

            switch (obj.CurrentSensorPosition)
            {
                case SensorPosition.Front:
                    break;
                case SensorPosition.Right:
                    obj.SensorContainer.RenderTransform = new RotateTransform(90, 50, 50);
                    break;
                case SensorPosition.Left:
                    obj.SensorContainer.RenderTransform = new RotateTransform(-90, 50, 50);
                    break;
                case SensorPosition.Back:
                    obj.SensorContainer.RenderTransform = new RotateTransform(180, 50, 50);
                    break;
                default:
                    break;
            }
        }

        public enum SensorPosition
        {
            Front = 0,
            Right = 1,
            Left = 2,
            Back = 3
        }



        public SensorPosition CurrentSensorPosition
        {
            get { return (SensorPosition)GetValue(CurrentSensorPositionProperty); }
            set { SetValue(CurrentSensorPositionProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CurrentSensorPosition.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CurrentSensorPositionProperty =
            DependencyProperty.Register("CurrentSensorPosition", typeof(SensorPosition), typeof(IRSensorRepresentation), new PropertyMetadata(SensorPosition.Front));




        private Brush GetSensorRepFillColour()
        {
            var val = Percentage(CurrentValue);
            if(val >= 75)
            {
                return Brushes.Green;
            }
            else if (val >= 50)
            {
                return Brushes.Yellow;
            }
            else if (val >= 25)
            {
                return Brushes.Orange;
            }
            else
            {
                return Brushes.Red;
            }
        }

        public float CurrentValue
        {
            get { return (float)GetValue(CurrentValueProperty); }
            set { SetValue(CurrentValueProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CurrentValue.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CurrentValueProperty =
            DependencyProperty.Register("CurrentValue", typeof(float), typeof(IRSensorRepresentation), new PropertyMetadata(0.0f, currMinMaxChanged));



        public float MinValue
        {
            get { return (float)GetValue(MinValueProperty); }
            set { SetValue(MinValueProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MinValue.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MinValueProperty =
            DependencyProperty.Register("MinValue", typeof(float), typeof(IRSensorRepresentation), new PropertyMetadata(0.0f, currMinMaxChanged));
        
        private float Percentage(float val)
        {
            return (val / (MaxValue - MinValue)) * 100.0f;
        }
    }
}

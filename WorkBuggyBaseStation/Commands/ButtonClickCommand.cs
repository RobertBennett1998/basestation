﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace BaseStation.Commands
{
    public class ButtonClickCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        private Action _function;
        public ButtonClickCommand(Action _func)
        {
            _function = _func;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            _function.Invoke();
        }
    }
}

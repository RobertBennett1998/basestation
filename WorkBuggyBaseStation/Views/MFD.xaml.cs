﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;

namespace WorkBuggyBaseStation.Views
{
    /// <summary>
    /// Interaction logic for MFD.xaml
    /// </summary>
    public partial class MFD : UserControl
    {

        private int _viewNumber = 0;

        public List<object> ViewsList
        {
            get { return (List<object>)GetValue(ViewsListProperty); }
            set { SetValue(ViewsListProperty, value); }
        }

        // Using a DependencyProperty as the backing store for VViewsList.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ViewsListProperty =
            DependencyProperty.Register("ViewsList", typeof(List<object>), typeof(MFD), new PropertyMetadata(new List<object>()));

        public int IndexForView
        {
            get { return (int)GetValue(IndexForViewProperty); }
            set { SetValue(IndexForViewProperty, value); }  
        }

        // Using a DependencyProperty as the backing store for IndexForView.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IndexForViewProperty =
            DependencyProperty.Register("IndexForView", typeof(int), typeof(MFD), new PropertyMetadata(0, ((o, args) =>
            {
                var obj = o as MFD;
                if(obj == null) return;

               obj.SetViewFromIndex((int)args.NewValue);

            })));
        
        public MFD()
        {
            InitializeComponent();
        }

        private void SetViewFromIndex(int index)
        {            
                if (index <= 0) return;

                if (index - 1 > ViewsList.Count)
                {
                    return;
                }

            try
            {
                ContentPresenterArea.Content = ViewsList[index - 1];
            }
            catch (Exception e)
            {
                // ignored
            }
        }

        private void UpBtn_Click(object sender, RoutedEventArgs e)
        {
            if (ViewsList != null)
            {

                if (_viewNumber == ViewsList.Count -1) _viewNumber = 0;
                else _viewNumber++;

                ContentPresenterArea.Content = ViewsList[_viewNumber];
            }


        }

        private void DownBtn_Click(object sender, RoutedEventArgs e)
        {
            if (ViewsList != null)
            {
                if (_viewNumber == 0) _viewNumber = ViewsList.Count - 1;
                else _viewNumber--;

                ContentPresenterArea.Content = ViewsList[_viewNumber];
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (ViewsList != null)
            {
                if (ViewsList.Count != 0)
                {
                    ContentPresenterArea.Content = ViewsList[0];
                }
            }

            SetViewFromIndex(IndexForView);
        }
    }
}

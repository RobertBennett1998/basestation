﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UserControlsCreation
{
    /// <summary>
    /// Interaction logic for ServoPanTiltControl.xaml
    /// </summary>
    public partial class ServoPanTiltControl : UserControl
    {




        public int TiltValue
        {
            get { return (int)GetValue(TiltValueProperty); }
            set { SetValue(TiltValueProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TiltValue.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TiltValueProperty =
            DependencyProperty.Register("TiltValue", typeof(int), typeof(ServoPanTiltControl), new PropertyMetadata(0,
                (o, args) =>
                {
                    var obj = o as ServoPanTiltControl;
                    obj.UpdateSliders();
                }));



        public int PanValue
        {
            get { return (int)GetValue(PanValueProperty); }
            set { SetValue(PanValueProperty, value); }
        }

        // Using a DependencyProperty as the backing store for PanValue.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PanValueProperty =
            DependencyProperty.Register("PanValue", typeof(int), typeof(ServoPanTiltControl), new PropertyMetadata(0,
                (o, args) =>
                {
                    var obj = o as ServoPanTiltControl;
                    obj.UpdateSliders();
                }));



        public void UpdateSliders()
        {
            TiltSlider.Value = TiltValue;
            PanSlider.Value = PanValue;
        }

            






        public ServoPanTiltControl()
        {
            InitializeComponent();
        }

        private void TiltSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            SetValue(TiltValueProperty, Convert.ToInt32(this.TiltSlider.Value));
        }

        private void PanSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            SetValue(PanValueProperty, Convert.ToInt32(this.PanSlider.Value));
        }
    }
}

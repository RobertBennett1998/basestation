﻿using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseStation.Utillity
{
    public static class Injector
    {
        public static SimpleInjector.Container Container { get; set; }
        public static void Init()
        {
            Container = new Container();
            Container.Verify();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BaseStation.Exceptions
{
    class BluetoothException : Exception
    {
        private BluetoothException()
        {
        }

        public BluetoothException(string message) : base(message)
        {
        }

        public BluetoothException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected BluetoothException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}

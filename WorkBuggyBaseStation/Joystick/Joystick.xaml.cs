﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace WorkBuggyBaseStation.Joystick
{
    /// <summary>
    /// Interaction logic for Joystick.xaml
    /// </summary>
    public partial class Joystick : UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        public static readonly DependencyProperty DebugProperty = DependencyProperty.Register("Debug", typeof(bool), typeof(Joystick), new PropertyMetadata(false, new PropertyChangedCallback(OnDebugChanged)));

        private bool _Debug = false;
        public bool Debug
        {
            get
            {
                return (bool)GetValue(DebugProperty);
            }
            set
            {
                SetValue(DebugProperty, value);
            }
        }

        private static void OnDebugChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Joystick joystick = d as Joystick;
            joystick._Debug = (bool)e.NewValue;
        }

        public Joystick()
        {
            InitializeComponent();

            _rootTwo = Math.Sqrt(2);
        }

        private Point MapPointFromCentreRef(Point fromCenter, Point Dimensions)
        {
            Point ret = new Point(0, 0);

            ret = fromCenter;
            ret.X += ((JoystickBackground.ActualWidth / 2) - (Dimensions.X / 2));
            ret.Y += ((JoystickBackground.ActualHeight / 2) - (Dimensions.Y / 2));

            return ret;
        }

        private Point MapPointToCentreRef(Point fromBL, Point Dimensions)
        {
            Point ret = new Point(0, 0);

            ret = fromBL;
            ret.X -= ((JoystickBackground.ActualWidth / 2));
            ret.Y -= ((JoystickBackground.ActualHeight / 2));
            ret.Y *= -1;
            return ret;
        }

        bool _bMoving = false;
        private void Controller_MouseDown(object sender, MouseButtonEventArgs e)
        {
            _bMoving = true;
        }

        private void Controller_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (_bMoving)
            {
                UpdatePos(new Point(JoystickBackground.ActualWidth / 2, JoystickBackground.ActualHeight / 2));
                _bMoving = false;
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (_Debug)
            {
                DebugText.Visibility = Visibility.Visible;

                Line hor = new Line();
                hor.Fill = Brushes.White;
                hor.X1 = 0;
                hor.X2 = JoystickBackground.ActualWidth;
                hor.Y1 = JoystickBackground.ActualHeight / 2;
                hor.Y2 = JoystickBackground.ActualHeight / 2;
                hor.Stroke = Brushes.White;
                hor.StrokeThickness = 1;
                hor.IsHitTestVisible = false;
                JoystickBackground.Children.Add(hor);

                Line vert = new Line();
                vert.Fill = Brushes.White;
                vert.X1 = JoystickBackground.ActualWidth / 2;
                vert.X2 = JoystickBackground.ActualWidth / 2;
                vert.Y1 = 0;
                vert.Y2 = JoystickBackground.ActualHeight;
                vert.Stroke = Brushes.White;
                vert.StrokeThickness = 1;
                vert.IsHitTestVisible = false;
                JoystickBackground.Children.Add(vert);
            }
            else
            {
                DebugText.Visibility = Visibility.Collapsed;
            }

            Point pos = MapPointFromCentreRef(new Point(0, 0), new Point(20, 20));
            Canvas.SetLeft(Controller, pos.X);
            Canvas.SetBottom(Controller, pos.Y);
        }

        private void Controller_MouseMove(object sender, MouseEventArgs e)
        {
            if (_bMoving)
            {
                UpdatePos(e.GetPosition(JoystickBackground));
            }
        }

        private Point _ControllerPos = new Point(0, 0);
        public Point ControllerPos
        {
            get
            {
                return _ControllerPos;
            }
        }

        public String ControllerPosAsString
        {
            get
            {
                return "X: " + ControllerPos.X + " Y: " + ControllerPos.Y;
            }
        }

        private double[] _MotorValues = new double[2] { 0, 0 };



        public sbyte LeftMotorThrottleValue
        {
            get { return (sbyte)GetValue(LeftMotorThrottleValueProperty); }
            set { SetValue(LeftMotorThrottleValueProperty, value); }
        }

        // Using a DependencyProperty as the backing store for LeftMotorThrottleValue.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LeftMotorThrottleValueProperty =
            DependencyProperty.Register("LeftMotorThrottleValue", typeof(sbyte), typeof(Joystick), new PropertyMetadata((sbyte)0));



        public sbyte RightMotorThrottleValue
        {
            get { return (sbyte)GetValue(RightMotorThrottleValueProperty); }
            set { SetValue(RightMotorThrottleValueProperty, value); }
        }

        // Using a DependencyProperty as the backing store for RightMotorThrottleValue.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty RightMotorThrottleValueProperty =
            DependencyProperty.Register("RightMotorThrottleValue", typeof(sbyte), typeof(Joystick), new PropertyMetadata((sbyte)0));




        public string MotorValuesAsString { get { return $"L: {LeftMotorThrottleValue} R: {RightMotorThrottleValue}"; } private set { return; } }

        //TODO: Add deadzone so forwards, backwards, left and right have a triangle from the center to the edge with a max of 5 wide

        private void UpdatePos(Point pos)
        {
            Point _pos = MapPointToCentreRef(new Point(Math.Floor(pos.X), Math.Floor(pos.Y)), new Point(20, 20));

            if ((_pos.X * _pos.X) + (_pos.Y * _pos.Y) > (100 * 100))
            {
                if (_pos.Y <= 0)
                {
                    _pos.X = Math.Floor(100 * -Math.Sin(Math.Atan(_pos.X / _pos.Y)));
                    _pos.Y = Math.Floor(100 * -Math.Cos(Math.Atan(_pos.X / _pos.Y)));
                }
                else
                {
                    _pos.X = Math.Floor(100 * Math.Sin(Math.Atan(_pos.X / _pos.Y)));
                    _pos.Y = Math.Floor(100 * Math.Cos(Math.Atan(_pos.X / _pos.Y)));
                }
            }

            pos = MapPointFromCentreRef(_pos, new Point(20, 20));

            Canvas.SetLeft(Controller, pos.X);
            Canvas.SetBottom(Controller, pos.Y);

            _ControllerPos = _pos;
            OnPropertyChanged("ControllerPos");
            DebugText.Content = ControllerPosAsString;

            CalculateMotorValues();
        }

        private readonly double _rootTwo = double.NaN;

        private int ClampMotorValue(int val)
        {
            if (val > 100)
                val = 100;

            if (val < -100)
                val = -100;

            return val;
        }

        void CalculateMotorValues()
        {
            double x = ControllerPos.X;
            double y = ControllerPos.Y;

            double r = Math.Sqrt((x * x) + (y * y));
            double theta = Math.Atan2(y, x);
            
            theta -= (Math.PI / 4);

            //System.Diagnostics.Debug.WriteLine(theta);

            LeftMotorThrottleValue = (sbyte)ClampMotorValue((int)((r * Math.Cos(theta)) * _rootTwo));

            RightMotorThrottleValue = (sbyte)ClampMotorValue((int)((r * Math.Sin(theta)) * _rootTwo));

            OnPropertyChanged("MotorValues");
            DebugTextA.Content = MotorValuesAsString;
        }
    }
}

﻿using BaseStation.Bluetooth.Messages;
using BaseStation.Bluetooth.Messages.Requests;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using WorkBuggyBaseStation.Bluetooth.Messages.Responses;

namespace WorkBuggyBaseStation.ViewModels
{
    class BuggyControllerViewModel
    {
        private BuggyControllerViewModel()
        {

        }
        private System.Timers.Timer _CanSendMotorParamsTimer = null;
        private Action<MessageBase> _sendMessageFunction = null;
        public BuggyControllerViewModel(Action<MessageBase> sendMessageFunction)
        {
            _sendMessageFunction = sendMessageFunction;

            _CanSendMotorParamsTimer = new System.Timers.Timer(50);
            _CanSendMotorParamsTimer.Elapsed += __CanSendMotorParamsTimer_Elapsed;
            _CanSendMotorParamsTimer.Start();
        }
        private bool _canSendMotorParams = false;
        private void __CanSendMotorParamsTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            _canSendMotorParams = true;
        }

        private sbyte _leftMotorThrottle;

        public sbyte LeftMotorThrottle
        {
            get { return _leftMotorThrottle; }
            set { _leftMotorThrottle = value; SendMotorCommand(); }
        }

        private sbyte _rightMotorThrottle;

        public sbyte RightMotorThrottle
        {
            get { return _rightMotorThrottle; }
            set { _rightMotorThrottle = value; SendMotorCommand(); }
        }

        private void SendMotorCommand()
        {
            byte lDir = 0;
            byte rDir = 0;
            byte lVal = (byte)LeftMotorThrottle;
            byte rVal = (byte)RightMotorThrottle;

            if (LeftMotorThrottle < 0)
            {
                lVal = (byte)-LeftMotorThrottle;
                lDir = 1;
            }

            if (RightMotorThrottle < 0)
            {
                rVal = (byte)-RightMotorThrottle;
                rDir = 1;
            }

            //Debug.WriteLine($"l: {lVal} r: {rVal}");
            if (_canSendMotorParams || (lVal == 0 && rVal == 0))
            {
                _sendMessageFunction.Invoke(new SetMotorParams(lVal, lDir, rVal, rDir));
                _canSendMotorParams = false;
            }
        }
    }
}

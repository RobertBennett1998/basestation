﻿using BaseStation.Bluetooth.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using WorkBuggyBaseStation.Bluetooth.Messages.Responses;

namespace WorkBuggyBaseStation.ViewModels
{
    class ServoPanTiltViewModel
    {
        private System.Timers.Timer _CanSendServoParamsTimer = null;
        private Action<MessageBase> _sendMessageFunction = null;
        public ServoPanTiltViewModel(Action<MessageBase> sendMessageFunction)
        {
            _sendMessageFunction = sendMessageFunction;

            _CanSendServoParamsTimer = new System.Timers.Timer(50);
            _CanSendServoParamsTimer.Elapsed += __CanSendServoParamsTimer_Elapsed;
            _CanSendServoParamsTimer.Start();
        }
        private bool _canSendServorParams = false;
        private void __CanSendServoParamsTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            _canSendServorParams = true;
        }


        private sbyte _panRelativeServoPos;
        public sbyte PanRelativeServoPos
        {
            get { return _panRelativeServoPos; }
            set { _panRelativeServoPos = value; ServoValuesChanged(); }
        }

        private void ServoValuesChanged()
        {
            if(_canSendServorParams)
            {
                _sendMessageFunction(new SetRelativeServoPostions(_panRelativeServoPos, _tiltRelativeServoPos));
            }
        }

        private sbyte _tiltRelativeServoPos;
        public sbyte TiltRelativeServoPos
        {
            get { return _tiltRelativeServoPos; }
            set { _tiltRelativeServoPos = value; }
        }

    }
}

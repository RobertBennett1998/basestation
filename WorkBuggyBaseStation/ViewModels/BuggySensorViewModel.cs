﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Net.Mime;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows;
using BaseStation.Bluetooth.EventArgs;
using BaseStation.Bluetooth.Messages;
using BaseStation.Bluetooth.Messages.Requests;
using WorkBuggyBaseStation.Bluetooth;
using WorkBuggyBaseStation.Bluetooth.Factories;

namespace WorkBuggyBaseStation.ViewModels
{
    class BuggySensorViewModel : INotifyPropertyChanged
    {
        private readonly Action<MessageBase> _sendMessageFunction;
        public BuggySensorViewModel(Action<MessageBase> sendMessageFunction)
        {
            _sendMessageFunction = sendMessageFunction;
            _RequestSensorDataTimer = new System.Timers.Timer(200);
            _RequestSensorDataTimer.Elapsed += _RequestSensorDataTimer_Elapsed;
            _RequestSensorDataTimer.Start();
            MessageManager.MessageRecieved += BluetoothMessageRecieved;
        }

        private void _RequestSensorDataTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (Application.Current != null)
            {
                Application.Current.Dispatcher.Invoke(() => _sendMessageFunction(new RequestSensorData()));
            }            
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            handler?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        private System.Timers.Timer _RequestSensorDataTimer;

        public void BluetoothMessageRecieved(object sender, MessageRecievedArgs e)
        {
            switch (e.MessageId)
            {
                case IncomingMessagesIds.Undefined:
                    break;
                case IncomingMessagesIds.SensorData:
                    FrontUSSensorValue = e.Message.GetField("FrontUS");
                    FrontIRSensorValue = e.Message.GetField("FrontIR");
                    RearIRSensorValue = e.Message.GetField("RearIR");
                    LeftIRSensorValue = e.Message.GetField("LeftIR");
                    RightIRSensorValue = e.Message.GetField("RightIR");
                    Debug.WriteLine($"Front US: {FrontUSSensorValue} Front: {FrontIRSensorValue} Rear: {RearIRSensorValue} Left: {LeftIRSensorValue}, Right: {RightIRSensorValue}");
                    break;

                default:
                    break;
            }
        }

        private int _frontIrSensorValue = 0;
        public int FrontIRSensorValue
        {
            get { return _frontIrSensorValue; }
            set { _frontIrSensorValue = value; OnPropertyChanged(nameof(FrontIRSensorValue)); }
        }

        private int _rearIrSensorValue = 0;
        public int RearIRSensorValue
        {
            get { return _rearIrSensorValue; }
            set { _rearIrSensorValue = value; OnPropertyChanged(nameof(RearIRSensorValue)); }
        }

        private int _leftIrSensorValue = 0;
        public int LeftIRSensorValue
        {
            get { return _leftIrSensorValue; }
            set { _leftIrSensorValue = value; OnPropertyChanged(nameof(LeftIRSensorValue)); }
        }

        private int _rightIrSensorValue = 0;
        public int RightIRSensorValue
        {
            get { return _rightIrSensorValue; }
            set { _rightIrSensorValue = value; OnPropertyChanged(nameof(RightIRSensorValue)); }
        }

        private int _frontUsSensorValue = 0;

        public int FrontUSSensorValue
        {
            get { return _frontUsSensorValue; }
            set { _frontUsSensorValue = value; }
        }

        private int _frontUsSensorAngle = 0;

        public int FrontUSSensorAngle
        {
            get { return _frontUsSensorAngle; }
            set { _frontUsSensorAngle = value; }
        }


    }
}

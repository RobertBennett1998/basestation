﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using WorkBuggyBaseStation.ViewModels;

namespace WorkBuggyBaseStation.ViewModels
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {

        private readonly BuggyConnectionViewModel _buggyConnectionViewModel;
        private readonly BuggySensorViewModel _buggySensorViewModel;
        private readonly ServoPanTiltViewModel _servoPanTiltViewModel;
        private readonly DiagnosticsViewModel _diagnosticsViewModel;
        private readonly BuggyControllerViewModel _buggyControllerViewModel;

        public MainWindowViewModel()
        {
            //init viewmodels
            _buggyConnectionViewModel = new BuggyConnectionViewModel();
            _buggySensorViewModel = new BuggySensorViewModel((msg) =>
            {
                _buggyConnectionViewModel.SendMessage(msg);
            });
            _diagnosticsViewModel = new DiagnosticsViewModel();
            _servoPanTiltViewModel = new ServoPanTiltViewModel((msg) =>
            {
                _buggyConnectionViewModel.SendMessage(msg);
            });
            _buggyControllerViewModel = new BuggyControllerViewModel((msg) =>
            {
                _buggyConnectionViewModel.SendMessage(msg);
            });

            _buggyConnectionViewModel.MessageRecieved += _buggySensorViewModel.BluetoothMessageRecieved;

            ListOfViews = new List<object>()
            {
                _buggySensorViewModel,
                _servoPanTiltViewModel,
                _diagnosticsViewModel,
                _buggyControllerViewModel
            };

            //set views if needed
            _topBarView = _buggyConnectionViewModel;
        }        

        private List<object> _listOfViews;

        public List<object> ListOfViews  
        {
            get { return _listOfViews; }
            set { _listOfViews = value; OnPropertyChanged(nameof(ListOfViews)); }
        }

        private object _topBarView;

        public object TopBarView
        {
            get { return _topBarView; }     
            set { _topBarView = value; }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            handler?.Invoke(this, new PropertyChangedEventArgs(name));
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Media;
using BaseStation.Bluetooth.EventArgs;
using BaseStation.Bluetooth.Messages;
using BaseStation.Bluetooth.Messages.Requests;
using BaseStation.Commands;
using BaseStation.Exceptions;
using WorkBuggyBaseStation.Bluetooth;
using WorkBuggyBaseStation.Bluetooth.Factories;
namespace WorkBuggyBaseStation.ViewModels
{
    class BuggyConnectionViewModel : INotifyPropertyChanged
    {
        public enum ConnectionStatuses
        { 
            Undefined = 0,
            Connected = 1,
            Failed = 2,
            Disconnected = 3
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            handler?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        private Bluetooth.Bluetooth _BluetoothConnection;

        public BuggyConnectionViewModel()
        {
            _RefreshPortsCommand = new ButtonClickCommand(RefreshPorts);
            _ConnectCommand = new ButtonClickCommand(Connect);
            _DisconnectCommand = new ButtonClickCommand(Disconnect);
            _selectedComPort = Bluetooth.Bluetooth.GetPortList().First();
            HelloCommand = new ButtonClickCommand(() => { MessageBox.Show("Hello"); });
            RefreshPorts();

            InstanceId = _sInstanceCount;
            _sInstanceCount++;
            RequestSensorButtonClickCommand = new ButtonClickCommand(RequestSensorData);
        }

        public event EventHandler<MessageRecievedArgs> MessageRecieved;

        #region ButtonCommands
        private ButtonClickCommand _RefreshPortsCommand;
        private void RefreshPorts()
        {
            ComPorts = Bluetooth.Bluetooth.GetPortList();
        }
        public ButtonClickCommand RefreshPortsCommand
        {
            get { return _RefreshPortsCommand; }
            private set { _RefreshPortsCommand = value; }
        }

        private ButtonClickCommand _ConnectCommand;
        private void Connect()
        {
            if (_BluetoothConnection == null)
            {
                try
                {
                    _BluetoothConnection = new Bluetooth.Bluetooth();
                    _BluetoothConnection.Initialize(SelectedComPort);
                    MessageManager.MessageRecieved += _BluetoothConnection_MessageRecieved;
                }
                catch (BluetoothException e)
                {
                    System.Diagnostics.Debug.Write("Exception caught: " + e.Message);
                    _connectionStatus = ConnectionStatuses.Failed;
                    OnPropertyChanged(nameof(ConnectionStatus));
                    OnPropertyChanged(nameof(ConnectionColour));

                    return;
                }

                _connectionStatus = ConnectionStatuses.Connected;
                OnPropertyChanged(nameof(ConnectionStatus));
                OnPropertyChanged(nameof(ConnectionColour));                
            }
            else
            {
                Disconnect();
                Connect();
            }
        }

        public ButtonClickCommand RequestSensorButtonClickCommand { get; set; }

        private void RequestSensorData()
        {
            //SendMessage(new SensorDataRequest());
        }

        public void SendMessage<T>(T msg) where T : MessageBase
        {
            _BluetoothConnection?.SendMessage(msg);
        }

        private void _BluetoothConnection_MessageRecieved(object sender, MessageRecievedArgs e)
        {
            
            MessageRecieved?.Invoke(sender, e);
            //if (e.MessageId == IncomingMessagesIds.IncomingIrSensorMessage)
            //{
            //    //Debug.WriteLine("Sensor Reponses");
            //    //string s =
            //        //$"Front: {e.Message.GetField("Front")} Left: {e.Message.GetField("Left")} Right: {e.Message.GetField("Back")} Back: {e.Message.GetField("Right")}";
            //   // Debug.WriteLine(s);
            //}
            //else if (e.MessageId != IncomingMessagesIds.IncomingCheckConnectionMessage)
            //{
            //    //Debug.WriteLine("Hmmmmm");
            //}
            
        }

        public ButtonClickCommand ConnectCommand
        {
            get { return _ConnectCommand; }
            private set { _ConnectCommand = value; }
        }

        public ButtonClickCommand HelloCommand
        {
            get;set;
        }

        private ButtonClickCommand _DisconnectCommand;
        private void Disconnect()
        {
            if (_BluetoothConnection != null)
            {
                _BluetoothConnection.Dispose();
                _BluetoothConnection = null;

                _connectionStatus = ConnectionStatuses.Disconnected;
                OnPropertyChanged(nameof(ConnectionStatus));
                OnPropertyChanged(nameof(ConnectionColour));
            }
        }
        public ButtonClickCommand DisconnectCommand
        {
            get { return _DisconnectCommand; }
            private set { _DisconnectCommand = value; }
        }
        #endregion

        private List<String> _comPorts = new List<String>();
        public List<String> ComPorts
        {
            get { return _comPorts; }
            private set { _comPorts = value; }
        }

        private String _selectedComPort = "Unselected";

        public String SelectedComPort
        {
            get { return _selectedComPort; }
            set { _selectedComPort = value; OnPropertyChanged(nameof(SelectedComPort)); }
        }

        private ConnectionStatuses _connectionStatus = ConnectionStatuses.Disconnected;
        public String ConnectionStatus
        {
            get { return Enum.GetName(typeof(ConnectionStatuses), _connectionStatus); }
            private set { return; }
        }

        private static int _sInstanceCount = 0;

        static public int InstanceCount
        {
            get {
                return _sInstanceCount;
                
            }
            private set { _sInstanceCount = value; }
        }

        private int _instanceId;

        public int InstanceId
        {
            get { return _instanceId; }
            set { _instanceId = value; }
        }


        public Brush ConnectionColour
        {
            get
            {
                switch (_connectionStatus)
                {
                    default:
                        return Brushes.HotPink;

                    case ConnectionStatuses.Connected:
                        return Brushes.LimeGreen;

                    case ConnectionStatuses.Failed:
                        return Brushes.Red;

                    case ConnectionStatuses.Disconnected:
                        return Brushes.Yellow;
                }
            }

            private set { }
        }
    }
}

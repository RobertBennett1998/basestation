﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkBuggyBaseStation.Bluetooth.Factories;

namespace BaseStation.Bluetooth.EventArgs
{
    public class MessageRecievedArgs : System.EventArgs
    {
        public Messages.MessageBase Message { get; set; }
        public IncomingMessagesIds MessageId { get; set; }
    }
}

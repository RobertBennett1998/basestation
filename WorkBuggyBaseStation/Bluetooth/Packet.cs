﻿using BaseStation.Bluetooth.Messages;

namespace WorkBuggyBaseStation.Bluetooth
{
    class Packet<T> where T : MessageBase
    {
        private Packet()
        {

        }

        public Packet(Header head, T msg, Footer foot)
        {
            Header = head;
            Message = msg;
            Footer = foot;

            if (Message.Bytes != null)
            {
                Bytes = new byte[Header.Bytes.Length + Message.Bytes.Length + Footer.Bytes.Length];
                Header.Bytes.CopyTo(Bytes, 0);
                Message.Bytes.CopyTo(Bytes, Header.Bytes.Length);
                Footer.Bytes.CopyTo(Bytes, Header.Bytes.Length + Message.Bytes.Length);
            }
            else
            {
                Bytes = new byte[Header.Bytes.Length + Footer.Bytes.Length];
                Header.Bytes.CopyTo(Bytes, 0);
                Footer.Bytes.CopyTo(Bytes, Header.Bytes.Length);
            }

        }

        public Header Header { get; set; }
        public T Message { get; set; }
        public Footer Footer { get; set; }

        public byte[] Bytes { get; private set; }
    }
}

﻿using System;
using System.Diagnostics;
using BaseStation.Bluetooth.Messages;
using BaseStation.Bluetooth.Messages.Requests;
using WorkBuggyBaseStation.Bluetooth.Messages.Responses;

namespace WorkBuggyBaseStation.Bluetooth.Factories
{
    public enum IncomingMessagesIds : byte
    {
        Undefined = 0,
        CheckConnection = 1,
        SensorData = 2
    }
    class IncomingMessageFactory
    {
        public MessageBase Create(byte msgId, byte[] msgBytes)
        {
            IncomingMessagesIds id = (IncomingMessagesIds)msgId;
            try
            {
                switch (id)
                {
                    case IncomingMessagesIds.Undefined:
                        return null;
                    case IncomingMessagesIds.CheckConnection:
                        return new CheckConnection(msgBytes);
                    case IncomingMessagesIds.SensorData:
                        return new SensorData(msgBytes);
                    default:
                        return null;
                }
            }
            catch (ArgumentException e)
            {
                Debug.WriteLine("Exception thrown " + e.Message);
                return null;
            }
        }
    }
}

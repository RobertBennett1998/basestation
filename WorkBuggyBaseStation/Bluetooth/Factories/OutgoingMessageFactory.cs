﻿using System;
using System.Diagnostics;
using BaseStation.Bluetooth.Messages;
using BaseStation.Bluetooth.Messages.Requests;
using WorkBuggyBaseStation.Bluetooth.Messages.Responses;

namespace WorkBuggyBaseStation.Bluetooth.Factories
{
    class OutgoingMessageFactory
    {
        public enum OutgoingMessageIds : byte
        {
            Undefined = 0,
            CheckConnection = 1,
            SetMotorParams = 2,
            RequestSensorData = 3,
            SetRelativeServoPositions = 4
        }

        public byte GetRequestLength(OutgoingMessageIds id)
        {
            switch (id)
            {
                case OutgoingMessageIds.Undefined:
                    break;
                case OutgoingMessageIds.CheckConnection:
                    return 1;
                case OutgoingMessageIds.SetMotorParams:
                    return 4;
                case OutgoingMessageIds.RequestSensorData:
                    return 0;
                case OutgoingMessageIds.SetRelativeServoPositions:
                    return 2;
                default:
                    break;
            }

            return 0;
        }

        public OutgoingMessageIds GetIdFromClass(Type t)
        {
            if (t == typeof(CheckConnection))
            {
                return OutgoingMessageIds.CheckConnection;
            }
            else if (t == typeof(SetMotorParams))
            {
                return OutgoingMessageIds.SetMotorParams;
            }
            else if (t == typeof(RequestSensorData))
            {
                return OutgoingMessageIds.RequestSensorData;
            }
            else if (t == typeof(SetRelativeServoPostions))
            {
                return OutgoingMessageIds.RequestSensorData;
            }
            return OutgoingMessageIds.Undefined;
        }

        public Packet<T> Create<T>(T msg) where T : MessageBase
        {
            OutgoingMessageIds id = GetIdFromClass(msg.GetType());
            Header head = new Header((byte)id, GetRequestLength(id));
            Footer foot = new Footer();

            return new Packet<T>(head, msg, foot);
        } 
    }
}

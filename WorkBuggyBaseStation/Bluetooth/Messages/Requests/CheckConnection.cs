﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseStation.Bluetooth.Messages.Requests
{
    class CheckConnection : MessageBase
    {
        public CheckConnection()
        {
            Identity = 0xFe;

            _bytes = new byte[1];
            _bytes[0] = Identity;
        }

        public CheckConnection(byte[] bytes)
        {
            if(bytes.Length != 1)
                throw new ArgumentException($"Incorrect length, expected 1, reciecved {bytes.Length}");

            Identity = bytes[0];

            _bytes = bytes;
        }

        public byte Identity { get; private set; }
    }
}

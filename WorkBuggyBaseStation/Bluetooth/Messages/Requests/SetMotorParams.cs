﻿using BaseStation.Bluetooth.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkBuggyBaseStation.Bluetooth.Messages.Responses
{
    class SetMotorParams : MessageBase
    {
        private SetMotorParams()
        {

        }

        public SetMotorParams(byte leftThrottle, byte leftDir, byte rightThrottle, byte rightDir)
        {
            LeftSpeedPercentage = leftThrottle;
            LeftDirection = leftDir;
            RightSpeedPercentage = rightThrottle;
            RightDirection = rightDir;

            _bytes = new byte[4];
            _bytes[0] = LeftSpeedPercentage;
            _bytes[1] = LeftDirection;
            _bytes[2] = RightSpeedPercentage;
            _bytes[3] = RightDirection;
        }

        public byte LeftSpeedPercentage { get; private set; }
        public byte LeftDirection { get; private set; }
        public byte RightSpeedPercentage { get; private set; }
        public byte RightDirection { get; private set; }
    }
}
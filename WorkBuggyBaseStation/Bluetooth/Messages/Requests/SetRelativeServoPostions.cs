﻿using BaseStation.Bluetooth.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkBuggyBaseStation.Bluetooth.Messages.Responses
{
    class SetRelativeServoPostions : MessageBase
    {
        private SetRelativeServoPostions()
        {

        }

        public SetRelativeServoPostions(sbyte panOffset, sbyte tiltOffset)
        {
            PanDegOffsetFromHomePos = panOffset;
            TiltDegOffsetFromHomePos = tiltOffset;

            _bytes = new byte[2];
            _bytes[0] = (byte)PanDegOffsetFromHomePos;
            _bytes[1] = (byte)TiltDegOffsetFromHomePos;
        }

        public sbyte PanDegOffsetFromHomePos { get; private set; }
        public sbyte TiltDegOffsetFromHomePos { get; private set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace BaseStation.Bluetooth.Messages
{
    public abstract class MessageBase
    {
        private readonly Dictionary<string, Feild> _fields = new Dictionary<string, Feild>();
        protected byte[] _bytes;
        public MessageBase()
        {
            foreach (var prop in this.GetType().GetProperties())
            {
                if (prop.Name != "Bytes")
                    _fields[prop.Name] = new Feild { Type = prop.GetValue(this).GetType(), PropInfo = prop };
            }
        }
        private struct Feild
        {
            public Type Type;
            public PropertyInfo PropInfo;
        }
       
        public T Cast<T>(object o)
        {
            return (T)o;
        }

        public dynamic GetField(string name)
        {
            Object o = null;
            if (!_fields.ContainsKey(name))
                throw new Exception("Field doens't exsist!");

            o = _fields[name].PropInfo.GetValue(this);

            MethodInfo castMethod = GetType().GetMethod("Cast")?.MakeGenericMethod(_fields[name].Type);

            return castMethod?.Invoke(this, new object[] { o });
        }
        
        public byte[] Bytes
        {
            get { return _bytes; }
            set { _bytes = value; }
        }

        
    }
}

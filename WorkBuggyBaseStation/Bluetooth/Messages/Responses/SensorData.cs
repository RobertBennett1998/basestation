﻿using BaseStation.Bluetooth.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkBuggyBaseStation.Bluetooth.Messages.Responses
{
    class SensorData : MessageBase
    {
        private SensorData()
        {

        }

        public SensorData(byte[] bytes)
        {
            Bytes = bytes;
            FrontUS = bytes[0];
            FrontIR = bytes[1];
            RearIR = bytes[2];
            LeftIR = bytes[3];
            RightIR = bytes[4];
        }

        public byte FrontUS { get; private set; }
        public byte FrontIR { get; private set; }
        public byte RearIR { get; private set; }
        public byte LeftIR { get; private set; }
        public byte RightIR { get; private set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseStation.Bluetooth.Messages
{
    class Header
    {
        public Header(byte msgId, byte msgLen)
        {
            Identity = (UInt16)(0xBA << 8 & 0xE0);
            MessageId = msgId;
            MessageLength = msgLen;

            Bytes = new byte[4];
            Bytes[0] = 0xBA;
            Bytes[1] = 0xE0;
            Bytes[2] = MessageId;
            Bytes[3] = MessageLength;
        }

        public Header(byte[] head)
        {
            byte[] header = new byte[2];
            header[1] = head[0];
            header[0] = head[1];
            Identity = BitConverter.ToUInt16(header, 0);
            if (Identity != 0xBAE0)
                throw new ArgumentException($"Header identity is invalid, expected 0xBAE0 (47840), recieved {Identity}");

            MessageId = head[2];
            MessageLength = head[3];

            Bytes = head;
        }

        public UInt16 Identity { get; set; }
        public byte MessageId { get; set; }
        public byte MessageLength { get; set; }

        public byte[] Bytes { get; private set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseStation.Bluetooth.Messages
{
    class Footer
    {
        public Footer()
        {
            Bytes = new byte[2];
            Bytes[0] = 0x0E;
            Bytes[1] = 0xAB;
        }

        public Footer(byte[] foot)
        {
            byte[] footer = new byte[2];
            footer[0] = foot[1];
            footer[1] = foot[0];
            Identity = BitConverter.ToUInt16(footer, 0);
            if(Identity != 0x0EAB)
                throw new ArgumentException($"Footer identity is invalid, expected 0x0EAB (3755), recieved {Identity}");

            Bytes = foot;
        }

        public UInt16 Identity { get; set; }
        public byte[] Bytes { get; private set; }
    }
}

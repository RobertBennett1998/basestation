﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BaseStation.Bluetooth.EventArgs;
using BaseStation.Bluetooth.Messages;
using BaseStation.Bluetooth.Messages.Requests;
using BaseStation.Exceptions;
using WorkBuggyBaseStation.Bluetooth.Factories;

namespace WorkBuggyBaseStation.Bluetooth
{
    class Bluetooth : IDisposable
    {
        private readonly object _lockable = new object();
        private readonly CancellationTokenSource _cancellationSource;
        private readonly CancellationToken _cancellationToken;
        private readonly IncomingMessageFactory _incomingMessageFactory;
        private readonly OutgoingMessageFactory _outgoingMessageFactory;

        private bool _readyForMessage;
        private SerialPort _serialPort;
        private ConcurrentQueue<byte[]> _packetQueue;        
        private Header _currentHeader;

        public Bluetooth()
        {
            _incomingMessageFactory = new IncomingMessageFactory();
            _outgoingMessageFactory = new OutgoingMessageFactory();
            _cancellationSource = new CancellationTokenSource();
            _cancellationToken = _cancellationSource.Token;

            MessageManager.MessageRecieved += BluetoothMessageRecieved;
        }

        #region Public Methods

        public static List<String> GetPortList()    
        {
            var ports = SerialPort.GetPortNames().ToList();
            if (ports.Count <= 0)
                ports.Add("Non Avaiable");

            return ports;
        }
             
        public void SendMessage<T>(T msg) where T : MessageBase
        {
            if (_packetQueue == null)
                _packetQueue = new ConcurrentQueue<byte[]>();
            var pck = _outgoingMessageFactory.Create<T>(msg).Bytes;

            if (pck[2] != 0x01)
                Debug.WriteLine($"Packet sent: {BaseStation.Utillity.Utillity.BytesToFormattedSting(pck)}");

            _packetQueue.Enqueue(pck);
        }

        public void Initialize(string portName)
        {
            _serialPort = new SerialPort(portName);
            try
            {
                _serialPort.Open();
            }
            catch (UnauthorizedAccessException e)
            {
                throw new BluetoothException("Couldn't open serial port", e);
            }
            catch (ArgumentException e)
            {
                throw new BluetoothException("Couldn't open serial port", e);
            }
            catch (IOException e)
            {
                throw new BluetoothException("Couldn't open serial port", e);
            }
            catch (InvalidOperationException e)
            {
                throw new BluetoothException("Couldn't open serial port", e);
            }

            _packetQueue = new ConcurrentQueue<byte[]>();
            Listen();
            Write();
        }

        #endregion

        #region Private Methods 
        private void MessageRecievedDispatch(Header header, MessageBase msg)
        {
            MessageRecievedArgs args = new MessageRecievedArgs() { Message = msg, MessageId = (IncomingMessagesIds)header.MessageId };
            MessageManager.Raise(this, args);
        }

        private void BluetoothMessageRecieved(object sender, MessageRecievedArgs e)
        {
            switch (e.MessageId)
            {
                case IncomingMessagesIds.CheckConnection:
                    if (e.Message.GetField("Identity") == 0xFE)
                    {
                        this.SendMessage(new CheckConnection());
                        //Debug.WriteLine("CC Recevied");
                    }
                    break;
            }
        }

        private void Listen()
        {
            var task = Task.Run(() =>
            {
                while (!_cancellationToken.IsCancellationRequested)
                {
                    lock (_lockable)
                    {
                        if (!_serialPort.IsOpen)
                            return;

                        if (!_readyForMessage && _serialPort.BytesToRead >= 4)
                        {
                            if (!ReadAndVerifyHeader(ref _currentHeader))
                                continue;

                            _readyForMessage = true;
                        }
                        else if (_readyForMessage && _currentHeader != null && _serialPort.BytesToRead >= (_currentHeader.MessageLength + 2))
                        {
                            MessageBase msg;
                            if (!ReadAndVerifyMessage(_currentHeader, out msg))
                            {
                                _currentHeader = null;
                                _readyForMessage = false;
                                continue;
                            }

                            MessageRecievedDispatch(_currentHeader, msg);

                            _currentHeader = null;
                            _readyForMessage = false;
                        }
                        else if (_serialPort.BytesToRead == 0)
                            continue;


                    }
                }

                System.Diagnostics.Debug.WriteLine("Listen thread exiting?!");
            }, _cancellationToken);
        }

        private void Write()
        {
            var task = Task.Run(() =>
            {
                while (!_cancellationToken.IsCancellationRequested)
                {
                    lock (_lockable)
                    {
                        if (!_serialPort.IsOpen)
                            return;

                        byte[] pck;
                        if (_packetQueue.TryDequeue(out pck))
                        {
                            _serialPort.Write(pck, 0, pck.Length);

                        }
                    }
                }

                System.Diagnostics.Debug.WriteLine("Listen thread exiting?!");
            }, _cancellationToken);
        }

        private bool ReadAndVerifyHeader(ref Header header)
        {
            byte[] headerBytes = new byte[4];
            _serialPort.Read(headerBytes, 0, 1);
            if (headerBytes[0] == 0xBA)
            {
                _serialPort.Read(headerBytes, 1, 1);
                if (headerBytes[1] == 0xE0)
                {
                    _serialPort.Read(headerBytes, 2, 2);
                    try
                    {
                        header = new Header(headerBytes);
                    }
                    catch (ArgumentException e)
                    {
                        Debug.WriteLine("Exception thrown " + e.Message);
                        return false;
                    }

                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

            return false;
        }

        private bool ReadAndVerifyMessage(Header header, out MessageBase msg)
        {
            byte[] msgBytes = new byte[header.MessageLength];
            _serialPort.Read(msgBytes, 0, header.MessageLength);

            byte[] footerBytes = new byte[2];
            _serialPort.Read(footerBytes, 0, 2);

            try
            {
                Footer footer = new Footer(footerBytes);
            }
            catch (ArgumentException e)
            {
                Debug.WriteLine("Exception thrown " + e.Message);
                msg = null;
                return false;
            }

            byte[] pck = new byte[header.Bytes.Length + msgBytes.Length + footerBytes.Length];

            header.Bytes.CopyTo(pck, 0);
            msgBytes.CopyTo(pck, header.Bytes.Length);
            footerBytes.CopyTo(pck, header.Bytes.Length + msgBytes.Length);

            msg = _incomingMessageFactory.Create(header.MessageId, msgBytes);

            if (msg != null)
                return true;

            return false;
        }
        #endregion

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (_serialPort.IsOpen)
                    {
                        _cancellationSource.Cancel();
                        lock (_lockable)
                        {
                            _serialPort.Close();
                            _serialPort.Dispose();
                        }
                    }
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~Bluetooth() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}

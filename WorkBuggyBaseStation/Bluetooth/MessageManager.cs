﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseStation.Bluetooth.EventArgs;

namespace WorkBuggyBaseStation.Bluetooth
{
    public static class MessageManager
    {
        public static event EventHandler<MessageRecievedArgs> MessageRecieved;

        public static void Raise(object sender, MessageRecievedArgs args)
        {
            MessageRecieved?.Invoke(sender, args);
        }
    }
}

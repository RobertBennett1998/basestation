﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WorkBuggyBaseStation.Shapes
{
    /// <summary>
    /// Interaction logic for Cone.xaml
    /// </summary>
    public partial class Cone : UserControl
    {
        public Cone()
        {
            InitializeComponent();
        }

        private Geometry CalculateConePath(double l, double r, double theta)
        {
            double w = l * Math.Sin((theta * Math.PI) / 180);
            //EXAMPLE CONE
            //M50,100 l-50,-70 c0,0 50,-30 100,0 l-50,70
            l = l - r;
            string path = null;
            string startPoint = "M" + (w / 2) + "," + (l + r);
            string leftLine = "l" + -(w / 2) + "," + -l;
            string curve = "c0,0 " //start point
                           + " " + (w / 2) + ", " + -r //mid point
                           + " " + (w) + ",0"; //endpoint
            string rightLine = "l" + -(w / 2) + "," + l;

            path = startPoint + " " + leftLine + " " + curve + " " + rightLine;

            return Geometry.Parse(path);
        }

        public float Length
        {
            get { return (float)GetValue(LengthProperty); }
            set { SetValue(LengthProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Length.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LengthProperty =
            DependencyProperty.Register("Length", typeof(float), typeof(Cone), new PropertyMetadata(100.0f, coneShapeChanged));



        public float Theta
        {
            get { return (float)GetValue(ThetaProperty); }
            set { SetValue(ThetaProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Width.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ThetaProperty =
            DependencyProperty.Register("Theta", typeof(float), typeof(Cone), new PropertyMetadata(45.0f, coneShapeChanged));



        public float CurveRadius
        {
            get { return (float)GetValue(CurveRadiusProperty); }
            set { SetValue(CurveRadiusProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CurveRadius.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CurveRadiusProperty =
            DependencyProperty.Register("CurveRadius", typeof(float), typeof(Cone), new PropertyMetadata(20.0f, coneShapeChanged));



        public Brush ConeOutlineColour
        {
            get { return (Brush)GetValue(ConeOutlineColourProperty); }
            set { SetValue(ConeOutlineColourProperty, value); }
        }

        // Using a DependencyProperty as the backing store for BorderStorke.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ConeOutlineColourProperty =
            DependencyProperty.Register("ConeOutlineColour", typeof(Brush), typeof(Cone), new PropertyMetadata(Brushes.Black));



        public int ConeOutlineThickness
        {
            get { return (int)GetValue(ConeOutlineThicknessProperty); }
            set { SetValue(ConeOutlineThicknessProperty, value); }
        }

        // Using a DependencyProperty as the backing store for BorderThickness.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ConeOutlineThicknessProperty =
            DependencyProperty.Register("ConeOutlineThickness", typeof(int), typeof(Cone), new PropertyMetadata(0, borderPropertiesChanged));

        private static void borderPropertiesChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var obj = d as Cone;
            if (obj == null)
                return;

            obj.ConePath.Stroke = obj.ConeOutlineColour;
            obj.ConePath.StrokeThickness = obj.ConeOutlineThickness;
        }

        public Brush ConeFill
        {
            get { return (Brush)GetValue(ConeFillProperty); }
            set { SetValue(ConeFillProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ConeFill.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ConeFillProperty =
            DependencyProperty.Register("ConeFill", typeof(Brush), typeof(Cone), new PropertyMetadata(Brushes.White, coneColourChanged));

        private static void coneColourChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var obj = d as Cone;
            if (obj == null)
                return;

            obj.ConePath.Fill = (Brush)e.NewValue;
        }

        private static void coneShapeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var obj = d as Cone;
            if (obj == null)
                return;

            if (obj.Length < 0)
                throw new ArgumentException($"Cone length is {obj.Length}, it must be greater than or equal to 0");

            if (obj.CurveRadius < 0)
                throw new ArgumentException($"Cone curve radius is {obj.CurveRadius}, length - curve radius must be greater than or equal to 0 it was {obj.Length - obj.CurveRadius}");

            if (obj.Theta == 0)
                throw new ArgumentException($"Cone theta is zero");

            obj.ConePath.Data = obj.CalculateConePath(obj.Length, obj.CurveRadius, obj.Theta);
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            var obj = this;

            if (obj.Length < 0)
                throw new ArgumentException($"Cone length is {obj.Length}, it must be greater than 0");

            if (obj.CurveRadius < 0)
                throw new ArgumentException($"Cone curve radius is {obj.CurveRadius}, length - curve radius must be greater than 0 it was {obj.Length - obj.CurveRadius}");

            if (obj.Theta == 0)
                throw new ArgumentException($"Cone theta is zero");

            obj.ConePath.Data = obj.CalculateConePath(obj.Length, obj.CurveRadius, obj.Theta);
        }
    }
}

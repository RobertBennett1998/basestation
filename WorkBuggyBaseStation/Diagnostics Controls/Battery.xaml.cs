﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace WorkBuggyBaseStation.Diagnostics_Controls
{
    /// <summary>
    /// Interaction logic for Battery.xaml
    /// </summary>
    public partial class Battery : UserControl
    {




        public int Percentage
        {
            get { return (int)GetValue(PercentageProperty); }
            set { SetValue(PercentageProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Percentage.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PercentageProperty =
            DependencyProperty.Register(nameof(Percentage), typeof(int), typeof(Battery), new PropertyMetadata(new PropertyChangedCallback(PercentageChanged)));

        private static void PercentageChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var obj = dependencyObject as Battery;

            if(obj == null) throw new InvalidCastException();

            int value = (int) dependencyPropertyChangedEventArgs.NewValue;    
                    
            obj.Draw();
        }


        public Battery()
        {            
            InitializeComponent();            
        }


        private void ProcessPercentage(int percentage)
        {
        }

        private void Draw()
        {
            double percentageTofill = 0;
            ClearAll();
            if (Percentage <= 25)
            {                 
                BottomBarBacker.Fill = new SolidColorBrush(Colors.Red);
                BottomMiddleBarBacker.Fill = null;
                TopMiddleBarBacker.Fill = null;
                TopBarBacker.Fill = null;
                percentageTofill = (Percentage / 25.0);
                int height = Convert.ToInt32(BottomBar.ActualHeight * percentageTofill);
                BottomBarBacker.Height = height;
                                              return;
            }

            if (Percentage <= 50)
            {
                this.BottomBar.Fill = new SolidColorBrush(Colors.Orange);
                percentageTofill = (Percentage - 25) / 25.0;
                int height = Convert.ToInt32(BottomMiddleBar.ActualHeight * percentageTofill);
                BottomMiddleBarBacker.Fill = new SolidColorBrush(Colors.Orange);
                BottomMiddleBarBacker.Height = height;
                return;
            }

            if (Percentage <= 75)
            {
                this.BottomBar.Fill = new SolidColorBrush(Colors.YellowGreen);
                this.BottomMiddleBar.Fill = new SolidColorBrush(Colors.YellowGreen);
                percentageTofill = (Percentage - 50) / 25.0;
                int height = Convert.ToInt32(TopMiddleBar.ActualHeight * percentageTofill);
                TopMiddleBarBacker.Fill = new SolidColorBrush(Colors.YellowGreen);
                TopMiddleBarBacker.Height = height;
                return;
            }

            if (percentageTofill <= 100)
            {
                this.BottomBar.Fill = new SolidColorBrush(Colors.Green);
                this.BottomMiddleBar.Fill = new SolidColorBrush(Colors.Green);
                this.TopMiddleBar.Fill = new SolidColorBrush(Colors.Green);
                percentageTofill = (Percentage - 75) / 25.0;
                int height = Convert.ToInt32(TopBar.ActualHeight * percentageTofill);
                TopBarBacker.Fill = new SolidColorBrush(Colors.Green);
                TopBarBacker.Height = height;
            }
        }

        private void ClearAll()
        {
            this.BottomBar.Fill = new SolidColorBrush(Colors.Gray);
            this.BottomBarBacker.Fill = null;
            this.BottomMiddleBar.Fill = new SolidColorBrush(Colors.Gray);
            this.BottomMiddleBarBacker.Fill = null;
            this.TopMiddleBarBacker.Fill = null;
            this.TopMiddleBar.Fill = new SolidColorBrush(Colors.Gray);
            this.TopBarBacker.Fill = null;
            this.TopBar.Fill = new SolidColorBrush(Colors.Gray);
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
             Draw();
        }
    }
}

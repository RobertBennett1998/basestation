﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace WorkBuggyBaseStation.Diagnostics_Controls
{
    /// <summary>
    /// Interaction logic for CurrentControl.xaml
    /// </summary>
    public partial class CurrentControl : UserControl
    {


        public double CurrentLevelValue
        {
            get { return (double)GetValue(CurrentLevelValueProperty); }
            set { SetValue(CurrentLevelValueProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CurrentLevelValue.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CurrentLevelValueProperty =
            DependencyProperty.Register("CurrentLevelValue", typeof(double), typeof(CurrentControl), new PropertyMetadata(0.0,
                (o, args) =>
                {
                    var obj = o as CurrentControl;
                    if (obj != null) obj.Fill();
                }));


        public void Fill()
        {
            this.CurrentLevel.Content = CurrentLevelValue + "Amps";
            var percentageToFill = CurrentLevelValue / 1.5;
            var heightToFill = this.CurrentRectangle.ActualHeight * percentageToFill;
            this.CurrentBacker.Fill = new SolidColorBrush(Colors.Aqua);
            this.CurrentBacker.Height = heightToFill;

        }

        public CurrentControl()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Fill();
        }
    }
}
